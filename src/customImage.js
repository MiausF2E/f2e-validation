const validateImage = (file, width, height) => {
  const URL = window.URL || window.webkitURL
  return new Promise(resolve => {
    const image = new Image()
    image.onerror = () => resolve({ valid: false })
    image.onload = () => {
      const valid = image.width <= Number(width) && image.height <= Number(height)
      resolve({ valid })
    }

    image.src = URL.createObjectURL(file)
  })
}

const validate = (files, [width, height]) => {
  const list = files
    .filter(file => /\.(jpg|svg|jpeg|png|bmp|gif)$/i.test(file.name))

  return Promise.all(list.map(file => validateImage(file, width, height)))
}

export default validate
