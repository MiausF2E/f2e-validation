import Vue from 'vue'
import Router from 'vue-router'
import CreateAccount from './views/CreateAccount.vue'
import GeneralInformation from './views/GeneralInformation.vue'
import ProfilePicture from './views/ProfilePicture.vue'
import PaymentMethod from './views/PaymentMethod.vue'
import Congratulations from './views/Congratulations.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/create-account',
      name: 'create-account',
      component: CreateAccount
    },
    {
      path: '/general-information',
      name: 'general-information',
      component: GeneralInformation
    },
    {
      path: '/profile-picture',
      name: 'profile-picture',
      component: ProfilePicture
    },
    {
      path: '/payment-method',
      name: 'payment-method',
      component: PaymentMethod
    },
    {
      path: '/congratulations',
      name: 'congratulations',
      component: Congratulations
    },
    {
      path: '/*',
      redirect: '/create-account'
    }
  ]
})
