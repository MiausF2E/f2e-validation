import Vue from 'vue'
import App from './App.vue'
import VueMq from 'vue-mq'
import VeeValidate, { Validator } from 'vee-validate'
import store from './store'
import router from './router'
import InputBox from './components/InputBox.vue'
import customImage from './customImage'

Validator.extend('customImage', customImage, {hasTarget: true})

Validator.localize({
  en: {
    messages: {
      alpha: 'Alphabetic characters only',
      credit_card: 'Card number is invalid',
      customImage: 'One file is over the maximum size',
      decimal: 'Numbers only',
      email: 'Invalid email',
      image: 'The file must be an image',
      is: 'Not match',
      length: 'wrong length',
      min: (field, ref) => `Minimum ${ref[0]} characters`,
      max: (field, ref) => `Maximum ${ref[0]} characters`,
      numeric: 'numeric only'
    }
  }
})

Vue.config.productionTip = false

Vue.use(VeeValidate)
Vue.use(VueMq, {
  breakpoints: {
    sm: 420,
    md: 768,
    lg: 1024,
    hd: Infinity
  }
})

Vue.component('input-box', InputBox)

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
